<?php

include_once "bd.inc.php";

function getAimerByMailU($mailU) {

	// A compléter - question 2.1 
    try 
   {
       $cnx = connexionPDO();
       $req = $cnx->prepare("select * from site_mvc.aimer inner join site_mvc.resto on id_r = id where mail=:mailU");
       $req->bindValue(':mailU', $mailU, PDO::PARAM_STR);

       $req->execute();

       $ligne = $req->fetch(PDO::FETCH_ASSOC);
        while ($ligne) {
            $resultat[] = $ligne;
            $ligne = $req->fetch(PDO::FETCH_ASSOC);
        }
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
    
}

function getAimerByIdR($idR) {

	// A compléter - question 2.1
    try 
   {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from site_mvc.aimer inner join site_mvc.resto on id_r = id  where id_r =:idR");
        $req->bindValue(':idR', $idR, PDO::PARAM_INT);

        $req->execute();

        $resultat = $req->fetch(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

function getAimerById($mailU, $idR){
    
	// A compléter - question 2.1
    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from site_mvc.aimer inner join site_mvc.resto on id_r = id  where mail =:mailU and id_r=:idR");
        $req->bindValue(':idR', $idR, PDO::PARAM_INT);
        $req->bindValue(':mailU', $mailU, PDO::PARAM_STR);

        $req->execute();

        $resultat = $req->fetch(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

function addAimer($mailU, $idR) {
    
	// A compléter - question 2.2
     try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("insert into site_mvc.aimer");
        $req->bindValue(':idR', $idR, PDO::PARAM_INT);
        $req->bindValue(':mailU', $mailU, PDO::PARAM_STR);

        $req->execute();

        $resultat = $req->fetch(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}




?>
